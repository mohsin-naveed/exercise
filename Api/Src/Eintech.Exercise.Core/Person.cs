﻿using System;

namespace Eintech.Exercise.Core
{
    public class Person
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime AddedOn { get; set; }
    }
}