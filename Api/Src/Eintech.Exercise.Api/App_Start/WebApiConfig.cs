﻿using Eintech.Exercise.Api.App_Start;
using Newtonsoft.Json.Serialization;
using System.Web.Http;

namespace Eintech.Exercise.Api
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {

            config.EnableCors();

            config.MapHttpAttributeRoutes();

            // Web API configuration and services
            AutofacConfig.Register();

            // Change Case of JSON
            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver =
              new CamelCasePropertyNamesContractResolver();

            //config.Routes.MapHttpRoute(
            //    name: "DefaultApi",
            //    routeTemplate: "api/{controller}/{id}",
            //    defaults: new { id = RouteParameter.Optional }
            //);
        }
    }
}
