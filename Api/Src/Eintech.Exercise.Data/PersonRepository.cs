﻿using Dapper.Contrib.Extensions;
using Eintech.Exercise.Core;
using Eintech.Exercise.Core.Interfaces;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Eintech.Exercise.Data
{
    public class PersonRepository : IPersonRepository
    {
        private IDbConnection _db;

        public PersonRepository(string connectionString)
        {
            _db = new SqlConnection(connectionString);
        }
        public Person Find(int id)
        {
            return _db.Get<Person>(id);
        }

        public IEnumerable<Person> GetAll()
        {
            return _db.GetAll<Person>();
        }
        public Person Save(Person person)
        {
            var id = _db.Insert(person);
            person.Id = (int)id;
            return person;
        }
    }
}