The solution requirements 

 1. Windows 10 64bit
 2. .NET Framework 4.7.2 (Web API 2)
 3. Visual Studio 2019
 4. Nunit, Moq
 5. Autofac for dependency injection.
 6. Dapper for Database operations 
 7. Angular 7 (Need Node and npm to install to run the project)
 8. SQL Server - Need to change the connection string 


 Api (ASP.NET Web Api 2 - C#)
 
 1. Exercise.Api
 2. Exercise.Core
 3. Exercise.Data
 4. Exercise.Tests
 
 Client (Angular 7, Bootstrap)



 - How to use.

 -- Open the solution in Visual Studio 2019 (.NET Framework 4.7.2)
 -- Restore the datbase from EintechDb.bak file
 -- Change the connection string from Exercise.Api project's web.config file to point to the correct database server.

 -- Let the solution install all the NuGet packages/dependencies.

 -- Run the project using a run button or Ctrl + F5.

 -- When Web Api is running - Client can be used.
 
 -- Open the Cliet project in the VS Code (Node and npm need to install to run the project)
 -- In the terminal window run command "npm install"  to download all packages
 -- Inter terminal window run command "ng serve -o" to run the project it will open the browser window. *** Web api need to run first **"



Please don't hesitate to contact me if you have any issues or need any more information regarding the solution.