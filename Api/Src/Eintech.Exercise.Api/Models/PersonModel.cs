﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Eintech.Exercise.Api.Models
{
    public class PersonModel
    {
        public PersonModel()
        {
            AddedOn = DateTime.Now;
        }

        [Required]
        public string Name { get; set; }
        public DateTime AddedOn { get; set; }
    }
}