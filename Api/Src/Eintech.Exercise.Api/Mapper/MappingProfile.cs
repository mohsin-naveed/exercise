﻿using AutoMapper;
using Eintech.Exercise.Api.Models;
using Eintech.Exercise.Core;
using System.Collections.Generic;

namespace Eintech.Exercise.Api.Mapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Person, PersonModel>().ReverseMap();

            //CreateMap<List<Person>, List<PersonModel>>().ReverseMap();
        }
    }
}