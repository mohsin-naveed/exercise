﻿using AutoMapper;
using Eintech.Exercise.Api.Controllers;
using Eintech.Exercise.Api.Models;
using Eintech.Exercise.Core;
using Eintech.Exercise.Core.Interfaces;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Web.Http.Results;

namespace Eintech.Exercise.Tests
{
    [TestFixture]
    public class PersonControllerTests
    {
        public Mock<IPersonRepository> mockPersonRepository;
        public Mock<IMapper> mockMapper;

        [SetUp]
        public void Setup()
        {
            mockPersonRepository = new Mock<IPersonRepository>();
            mockMapper = new Mock<IMapper>();
        }

        [TearDown]
        public void TearDown()
        {
            mockPersonRepository = null;
            mockMapper = null;
        }

        [Test]
        public void Post_WhenCalled_ShouldSetLocationHeader()
        {
            mockPersonRepository.Setup(x => x.Save(It.IsAny<Person>())).Returns(new Person { Id = 1, Name = "Mohsin" });
            var controller = new PersonsController(mockPersonRepository.Object, mockMapper.Object);
            var model = new PersonModel { Name = "Mohsin" };

            var actionResult = controller.Post(model);
            var createdResult = actionResult as CreatedAtRouteNegotiatedContentResult<PersonModel>;

            Assert.IsNotNull(createdResult);
            Assert.AreEqual("GetPerson", createdResult.RouteName);
            Assert.AreEqual(1, createdResult.RouteValues["Id"]);
        }

        [Test]
        public void Post_WhenCalled_ReturnsExceptionResult()
        {
            mockPersonRepository.Setup(x => x.Save(It.IsAny<Person>())).Throws<Exception>();
            var controller = new PersonsController(mockPersonRepository.Object, mockMapper.Object);
            var model = new PersonModel { Name = "Mohsin" };

            Assert.IsInstanceOf<ExceptionResult>(controller.Post(model));
        }

        [Test]
        public void Get_WhenCalled_ShouldReturnNotFound()
        {

            var controller = new PersonsController(mockPersonRepository.Object, mockMapper.Object);
            mockPersonRepository.Setup(x => x.Find(It.IsAny<int>()));
            var model = new PersonModel { Name = "Mohsin" };

            var actionResult = controller.Get(1000);

            Assert.IsInstanceOf<NotFoundResult>(actionResult);
        }

        [Test]
        public void Get_WhenCalledWithId_ShouldReturnOkResult()
        {
            mockPersonRepository.Setup(x => x.Find(It.IsAny<int>())).Returns(new Person { Id = 1, Name = "Mohsin" });
            var controller = new PersonsController(mockPersonRepository.Object, mockMapper.Object);
            var model = new PersonModel { Name = "Mohsin" };

            var actionResult = controller.Get(1);

            Assert.IsInstanceOf<OkNegotiatedContentResult<PersonModel>>(actionResult);
        }

        [Test]
        public void Get_WhenCalledWithoutId_ShouldReturnOkResult()
        {
            var persons = new List<Person>
            {
                new Person { Name = "Person1" },
                new Person { Name = "Person2" }
            };
            mockPersonRepository.Setup(x => x.GetAll()).Returns(persons);
            var controller = new PersonsController(mockPersonRepository.Object, mockMapper.Object);

            var actionResult = controller.Get();

            Assert.IsInstanceOf<OkNegotiatedContentResult<IEnumerable<PersonModel>>>(actionResult);
        }

        [Test]
        public void Post_WhenModelStateIsInvalid_ReturnBadRequest()
        {
            mockPersonRepository.Setup(x => x.Save(It.IsAny<Person>())).Throws<Exception>();
            var controller = new PersonsController(mockPersonRepository.Object, mockMapper.Object);
            controller.ModelState.AddModelError("Name", "");
            var model = new PersonModel { };

            var actionResult = controller.Post(model);

            Assert.IsInstanceOf<InvalidModelStateResult>(controller.Post(model));
        }
    }
}