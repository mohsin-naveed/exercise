
export interface IPersonList {
    name: string
    addedOn: Date
}