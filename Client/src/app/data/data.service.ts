import { Injectable } from '@angular/core';
import { IPerson } from './IPerson';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }

  postPerson(person: IPerson) : Observable<any> {
    
    return this.http.post('https://localhost:44355/api/persons', person);    
  }

  getAll() : Observable<any> {
    
    return this.http.get('https://localhost:44355/api/persons');    
  }
}
