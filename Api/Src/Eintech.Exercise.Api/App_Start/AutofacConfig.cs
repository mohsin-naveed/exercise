﻿using Autofac;
using Autofac.Integration.WebApi;
using AutoMapper;
using Eintech.Exercise.Api.Mapper;
using Eintech.Exercise.Core.Interfaces;
using Eintech.Exercise.Data;
using System.Reflection;
using System.Web.Http;

namespace Eintech.Exercise.Api.App_Start
{
    public class AutofacConfig
    {
        public static void Register()
        {
            var builder = new ContainerBuilder();
            var config = GlobalConfiguration.Configuration;
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            RegisterServices(builder);
            builder.RegisterWebApiFilterProvider(config);
            builder.RegisterWebApiModelBinderProvider();
            var container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }

        private static void RegisterServices(ContainerBuilder bldr)
        {


            var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ToString();

            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new MappingProfile());
            });

            bldr.RegisterInstance(config.CreateMapper())
              .As<IMapper>()
              .SingleInstance();

            bldr.RegisterType<PersonRepository>()
            .As<IPersonRepository>().WithParameter("connectionString", connectionString)
            .InstancePerRequest();
        }
    }
}