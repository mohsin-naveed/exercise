﻿using System.Collections.Generic;

namespace Eintech.Exercise.Core.Interfaces
{
    public interface IPersonRepository
    {
        Person Save(Person person);

        Person Find(int id);

        IEnumerable<Person> GetAll();
    }
}