import { Component, OnInit } from '@angular/core';
import { IPerson } from '../data/IPerson';
import { NgForm, NgModel } from '@angular/forms';
import { DataService } from '../data/data.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.css']
})
export class PersonComponent implements OnInit {

  personList: Observable<IPerson[]>;
  
  person : IPerson = {
    name: null
  };

  ngOnInit() {
    this.dataService.getAll().subscribe(
      result => this.personList = result,
      error => this.onHttpError(error)
    );
  }
  
  constructor(private dataService: DataService) { }

  onHttpError(errorResponse: any) {
    console.log('error: ', errorResponse);
  }

  onSuccess() {
    this.ngOnInit();
    this.person.name = null;
  }

  onSubmit(form: NgForm) {
    if (form.valid) {
      this.dataService.postPerson(this.person).subscribe(
        result => this.onSuccess(),
        error => this.onHttpError(error)
      );
    }
  }
}
