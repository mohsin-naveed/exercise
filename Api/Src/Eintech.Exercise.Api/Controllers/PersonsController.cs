﻿using AutoMapper;
using Eintech.Exercise.Api.Models;
using Eintech.Exercise.Core;
using Eintech.Exercise.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Eintech.Exercise.Api.Controllers
{
    [EnableCors("http://localhost:4200", "*", "*")]
    [RoutePrefix("api/persons")]
    public class PersonsController : ApiController
    {
        private readonly IPersonRepository _repository;
        private readonly IMapper _mapper;

        public PersonsController(IPersonRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        [Route()]
        public IHttpActionResult Get()
        {
            try
            {
                var persons = _repository.GetAll();
                var pesonModel = _mapper.Map<IEnumerable<Person>, IEnumerable<PersonModel>>(persons);

                return Ok(pesonModel);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [Route("{id:int}", Name = "GetPerson")]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var result = _repository.Find(id);

                if (result == null)
                {
                    return NotFound();
                }

                return Ok(_mapper.Map<PersonModel>(result));
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [Route()]
        public IHttpActionResult Post(PersonModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var person = _mapper.Map<Person>(model);

                    var newPerson = _repository.Save(person);

                    if (newPerson != null)
                    {
                        var newModel = _mapper.Map<PersonModel>(person);

                        return CreatedAtRoute("GetPerson", new { id = newPerson.Id }, newModel);
                    }
                }

                return BadRequest(ModelState);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
